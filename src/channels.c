#include "merc.h"
#include "interp.h"
#include "recycle.h"
#include "tables.h"
#include "channels.h"

const struct channel_type channel_table[] = {
  { "IMM", C_B_BLUE, COMM_NOWIZ },
  { "AUCTION", C_B_RED, COMM_NOAUCTION },
  { "GOSSIP", C_B_GREEN, COMM_NOGOSSIP },
  { "QUESTION", C_B_BLUE, COMM_NOQUESTION },
  { "MUSIC", C_B_BLUE, COMM_NOMUSIC },
  { "CLAN", C_B_BLUE, COMM_NOCLAN },
  { "QUOTE", C_B_BLUE, COMM_NOQUOTE },
  { "GRATS", C_B_MAGENTA, COMM_NOGRATS },
  { "SHOUTS", C_B_WHITE, COMM_SHOUTSOFF },
};

/* RT code to display channel status */
DO_FUNCTION(do_channels) {
    int ind;

    /* lists all channels and their status */
    send_to_char("   channel           status\r\n",ch);
    send_to_char("---------------------------\r\n",ch);

    for ( ind = 0; ind < MAX_CHANNELS; ++ind ) {
	if ( channel_table[ind].bit == COMM_NOWIZ
	&&   !IS_IMMORTAL(ch) )
	    continue;

	send_to_charf(ch, "%-20.20s %s\r\n",
	    channel_table[ind].name,
	    !IS_SET(ch->comm, channel_table[ind].bit) ? "ON" : "OFF");
    }

    send_to_charf(ch, "QUIET MODE           %s\r\n",
	IS_SET(ch->comm, channel_table[ind].bit) ? "ON" : "OFF");

    if (IS_SET(ch->comm,COMM_AFK))
        send_to_char("You are AFK.\r\n",ch);

    if (IS_SET(ch->comm,COMM_SNOOP_PROOF))
        send_to_char("You are immune to snooping.\r\n",ch);

    if (ch->lines != PAGELEN) {
        if (ch->lines) {
            send_to_charf(ch, "You display %d lines of scroll.\r\n", ch->lines + 2);
        }
        else
            send_to_char("Scroll buffering is off.\r\n",ch);
    }

    if (ch->prompt != NULL)
    {
        send_to_charf(ch, "Your current prompt is: %s\r\n", ch->prompt);
    }

    if (IS_SET(ch->comm,COMM_NOCHANNELS))
     send_to_char("You cannot use channels.\r\n",ch);

    if (IS_SET(ch->comm,COMM_NOSHOUT))
      send_to_char("You cannot shout.\r\n",ch);

    if (IS_SET(ch->comm,COMM_NOTELL))
      send_to_char("You cannot use tell.\r\n",ch);


    if (IS_SET(ch->comm,COMM_NOEMOTE))
      send_to_char("You cannot show emotions.\r\n",ch);
}

DO_FUNCTION(do_deaf)
{

   if (IS_SET(ch->comm,COMM_DEAF))
   {
     send_to_char("You can now hear tells again.\r\n",ch);
     REMOVE_BIT(ch->comm,COMM_DEAF);
   }
   else
   {
     send_to_char("From now on, you won't hear tells.\r\n",ch);
     SET_BIT(ch->comm,COMM_DEAF);
   }
}

/* RT quiet blocks out all communication */

DO_FUNCTION(do_quiet)
{
    if (IS_SET(ch->comm,COMM_QUIET))
    {
      send_to_char("Quiet mode removed.\r\n",ch);
      REMOVE_BIT(ch->comm,COMM_QUIET);
    }
   else
   {
     send_to_char("From now on, you will only hear says and emotes.\r\n",ch);
     SET_BIT(ch->comm,COMM_QUIET);
   }
}

DO_FUNCTION(do_afk)
{
    if (IS_SET(ch->comm,COMM_AFK))
    {
      send_to_char("AFK mode removed. Type 'replay' to see tells.\r\n",ch);
      REMOVE_BIT(ch->comm,COMM_AFK);
    }
   else
   {
     send_to_char("You are now in AFK mode.\r\n",ch);
     SET_BIT(ch->comm,COMM_AFK);
   }
}

DO_FUNCTION(do_replay)
{
    if (IS_NPC(ch))
    {
        send_to_char("You can't replay.\r\n",ch);
        return;
    }

    if (buf_string(ch->pcdata->buffer)[0] == '\0')
    {
        send_to_char("You have no tells to replay.\r\n",ch);
        return;
    }

    page_to_char(buf_string(ch->pcdata->buffer),ch);
    clear_buf(ch->pcdata->buffer);
}

DO_FUNCTION(do_auction)
{
    char buf[MAX_STRING_LENGTH];
    DESCRIPTOR_DATA *d;

    if (argument[0] == '\0' )
    {
      if (IS_SET(ch->comm,COMM_NOAUCTION))
      {
        send_to_char("Auction channel is now ON.\r\n",ch);
        REMOVE_BIT(ch->comm,COMM_NOAUCTION);
     }
      else
      {
        send_to_char("Auction channel is now OFF.\r\n",ch);
        SET_BIT(ch->comm,COMM_NOAUCTION);
      }
    }
    else  /* auction message sent, turn auction on if it is off */
    {
        if (IS_SET(ch->comm,COMM_QUIET))
        {
          send_to_char("You must turn off quiet mode first.\r\n",ch);
          return;
        }

        if (IS_SET(ch->comm,COMM_NOCHANNELS))
        {
          send_to_char("The gods have revoked your channel priviliges.\r\n",ch);
          return;
        }

        REMOVE_BIT(ch->comm,COMM_NOAUCTION);
    }

    sprintf( buf, "{yYou auction '%s'{x\r\n", argument );
    send_to_char( buf, ch );
    for ( d = descriptor_list; d != NULL; d = d->next )
    {
        CHAR_DATA *victim;

        victim = d->original ? d->original : d->character;

        if ( d->connected == CON_PLAYING &&
            d->character != ch &&
             !IS_SET(victim->comm,COMM_NOAUCTION) &&
             !IS_SET(victim->comm,COMM_QUIET) )
        {
            act_new("{y$n auctions '$t'{x",
                    ch,argument,d->character,TO_VICT,POS_DEAD);
        }
    }
}

DO_FUNCTION(do_gossip)
{
    if (argument[0] == '\0' )
    {
      if (IS_SET(ch->comm,COMM_NOGOSSIP))
      {
        send_to_char("Gossip channel is now ON.\r\n",ch);
        REMOVE_BIT(ch->comm,COMM_NOGOSSIP);
      }
      else
      {
        send_to_char("Gossip channel is now OFF.\r\n",ch);
        SET_BIT(ch->comm,COMM_NOGOSSIP);
      }
    }
    else  /* gossip message sent, turn gossip on if it isn't already */
        talk_channel(ch, argument, CHANNEL_GOSSIP);
}

DO_FUNCTION(do_grats)
{
    char buf[MAX_STRING_LENGTH];
    DESCRIPTOR_DATA *d;

    if (argument[0] == '\0' )
    {
      if (IS_SET(ch->comm,COMM_NOGRATS))
      {
        send_to_char("Grats channel is now ON.\r\n",ch);
        REMOVE_BIT(ch->comm,COMM_NOGRATS);
      }
      else
      {
        send_to_char("Grats channel is now OFF.\r\n",ch);
        SET_BIT(ch->comm,COMM_NOGRATS);
      }
    }
    else  /* grats message sent, turn grats on if it isn't already */
    {
        if (IS_SET(ch->comm,COMM_QUIET))
        {
          send_to_char("You must turn off quiet mode first.\r\n",ch);
          return;
        }

        if (IS_SET(ch->comm,COMM_NOCHANNELS))
        {
          send_to_char("The gods have revoked your channel priviliges.\r\n",ch);
          return;

        }

      REMOVE_BIT(ch->comm,COMM_NOGRATS);

      sprintf( buf, "You grats '%s'\r\n", argument );
      send_to_char( buf, ch );
      for ( d = descriptor_list; d != NULL; d = d->next )
      {
        CHAR_DATA *victim;

        victim = d->original ? d->original : d->character;

        if ( d->connected == CON_PLAYING &&
             d->character != ch &&
             !IS_SET(victim->comm,COMM_NOGRATS) &&
             !IS_SET(victim->comm,COMM_QUIET) )
        {
          act_new( "$n grats '$t'",
                   ch,argument, d->character, TO_VICT,POS_SLEEPING );
        }
      }
    }
}

DO_FUNCTION(do_quote)
{
    char buf[MAX_STRING_LENGTH];
    DESCRIPTOR_DATA *d;

    if (argument[0] == '\0' )
    {
      if (IS_SET(ch->comm,COMM_NOQUOTE))
      {
        send_to_char("Quote channel is now ON.\r\n",ch);
        REMOVE_BIT(ch->comm,COMM_NOQUOTE);
      }
      else
      {
        send_to_char("Quote channel is now OFF.\r\n",ch);
        SET_BIT(ch->comm,COMM_NOQUOTE);
      }
    }
    else  /* quote message sent, turn quote on if it isn't already */
    {
        if (IS_SET(ch->comm,COMM_QUIET))
        {
          send_to_char("You must turn off quiet mode first.\r\n",ch);
          return;
        }

        if (IS_SET(ch->comm,COMM_NOCHANNELS))
        {
          send_to_char("The gods have revoked your channel priviliges.\r\n",ch);
          return;

        }

      REMOVE_BIT(ch->comm,COMM_NOQUOTE);

      sprintf( buf, "You quote '%s'\r\n", argument );
      send_to_char( buf, ch );
      for ( d = descriptor_list; d != NULL; d = d->next )
      {
        CHAR_DATA *victim;

        victim = d->original ? d->original : d->character;

        if ( d->connected == CON_PLAYING &&
             d->character != ch &&
             !IS_SET(victim->comm,COMM_NOQUOTE) &&
             !IS_SET(victim->comm,COMM_QUIET) )
        {
          act_new( "$n quotes '$t'",
                   ch,argument, d->character, TO_VICT,POS_SLEEPING );
        }
      }
    }
}

DO_FUNCTION(do_question)
{
    char buf[MAX_STRING_LENGTH];
    DESCRIPTOR_DATA *d;

    if (argument[0] == '\0' )
    {
      if (IS_SET(ch->comm,COMM_NOQUESTION))
      {
        send_to_char("Q/A channel is now ON.\r\n",ch);
        REMOVE_BIT(ch->comm,COMM_NOQUESTION);
      }
      else
      {
        send_to_char("Q/A channel is now OFF.\r\n",ch);
        SET_BIT(ch->comm,COMM_NOQUESTION);
      }
    }
    else  /* question sent, turn Q/A on if it isn't already */
    {
        if (IS_SET(ch->comm,COMM_QUIET))
        {
          send_to_char("You must turn off quiet mode first.\r\n",ch);
          return;
        }

        if (IS_SET(ch->comm,COMM_NOCHANNELS))
        {
          send_to_char("The gods have revoked your channel priviliges.\r\n",ch);
          return;
        }

        REMOVE_BIT(ch->comm,COMM_NOQUESTION);

      sprintf( buf, "{yYou question '%s'{x\r\n", argument );
      send_to_char( buf, ch );
      for ( d = descriptor_list; d != NULL; d = d->next )
      {
        CHAR_DATA *victim;

        victim = d->original ? d->original : d->character;

        if ( d->connected == CON_PLAYING &&
             d->character != ch &&
             !IS_SET(victim->comm,COMM_NOQUESTION) &&
             !IS_SET(victim->comm,COMM_QUIET) )
        {
          act_new("{y$n questions '$t'{x",
                  ch,argument,d->character,TO_VICT,POS_SLEEPING);
        }
      }
    }
}

DO_FUNCTION(do_answer)
{
    char buf[MAX_STRING_LENGTH];
    DESCRIPTOR_DATA *d;

    if (argument[0] == '\0' )
    {
      if (IS_SET(ch->comm,COMM_NOQUESTION))
      {
        send_to_char("Q/A channel is now ON.\r\n",ch);
        REMOVE_BIT(ch->comm,COMM_NOQUESTION);
      }
      else
      {
        send_to_char("Q/A channel is now OFF.\r\n",ch);
        SET_BIT(ch->comm,COMM_NOQUESTION);
      }
    }
    else  /* answer sent, turn Q/A on if it isn't already */
    {
        if (IS_SET(ch->comm,COMM_QUIET))
        {
          send_to_char("You must turn off quiet mode first.\r\n",ch);
          return;
        }

        if (IS_SET(ch->comm,COMM_NOCHANNELS))
        {
          send_to_char("The gods have revoked your channel priviliges.\r\n",ch);
          return;
        }

        REMOVE_BIT(ch->comm,COMM_NOQUESTION);

      sprintf( buf, "{yYou answer '%s'{x\r\n", argument );
      send_to_char( buf, ch );
      for ( d = descriptor_list; d != NULL; d = d->next )
      {
        CHAR_DATA *victim;

        victim = d->original ? d->original : d->character;

        if ( d->connected == CON_PLAYING &&
             d->character != ch &&
             !IS_SET(victim->comm,COMM_NOQUESTION) &&
             !IS_SET(victim->comm,COMM_QUIET) )
        {
          act_new("{y$n answers '$t'{x",
                  ch,argument,d->character,TO_VICT,POS_SLEEPING);
        }
      }
    }
}

DO_FUNCTION(do_music)
{
    char buf[MAX_STRING_LENGTH];
    DESCRIPTOR_DATA *d;

    if (argument[0] == '\0' )
    {
      if (IS_SET(ch->comm,COMM_NOMUSIC))
      {
        send_to_char("Music channel is now ON.\r\n",ch);
        REMOVE_BIT(ch->comm,COMM_NOMUSIC);
      }
      else
      {
        send_to_char("Music channel is now OFF.\r\n",ch);
        SET_BIT(ch->comm,COMM_NOMUSIC);
      }
    }
    else  /* music sent, turn music on if it isn't already */
    {
        if (IS_SET(ch->comm,COMM_QUIET))
        {
          send_to_char("You must turn off quiet mode first.\r\n",ch);
          return;
        }

        if (IS_SET(ch->comm,COMM_NOCHANNELS))
        {
          send_to_char("The gods have revoked your channel priviliges.\r\n",ch);
          return;
        }

        REMOVE_BIT(ch->comm,COMM_NOMUSIC);

      sprintf( buf, "{yYou MUSIC: '%s'{x\r\n", argument );
      send_to_char( buf, ch );
      sprintf( buf, "{y$n MUSIC: '%s'{x", argument );
      for ( d = descriptor_list; d != NULL; d = d->next )
      {
        CHAR_DATA *victim;

        victim = d->original ? d->original : d->character;

        if ( d->connected == CON_PLAYING &&
             d->character != ch &&
             !IS_SET(victim->comm,COMM_NOMUSIC) &&
             !IS_SET(victim->comm,COMM_QUIET) )
        {
            act_new("{y$n MUSIC: '$t'{x",
                    ch,argument,d->character,TO_VICT,POS_SLEEPING);
        }
      }
    }
}

DO_FUNCTION(do_clantalk)
{
    char buf[MAX_STRING_LENGTH];
    DESCRIPTOR_DATA *d;

    if (!is_clan(ch) || clan_table[ch->clan].independent)
    {
        send_to_char("You aren't in a clan.\r\n",ch);
        return;
    }
    if ( argument[0] == '\0' )
    {
      if (IS_SET(ch->comm,COMM_NOCLAN))
      {
        send_to_char("Clan channel is now ON\r\n",ch);
        REMOVE_BIT(ch->comm,COMM_NOCLAN);
      }
      else
      {
        send_to_char("Clan channel is now OFF\r\n",ch);
        SET_BIT(ch->comm,COMM_NOCLAN);
      }
      return;
    }

        if (IS_SET(ch->comm,COMM_NOCHANNELS))
        {
         send_to_char("The gods have revoked your channel priviliges.\r\n",ch);
          return;
        }

        REMOVE_BIT(ch->comm,COMM_NOCLAN);

      sprintf( buf, "You clan '%s'\r\n", argument );
      send_to_char( buf, ch );
      sprintf( buf, "$n clans '%s'", argument );
    for ( d = descriptor_list; d != NULL; d = d->next )
    {
        if ( d->connected == CON_PLAYING &&
             d->character != ch &&
             is_same_clan(ch,d->character) &&
             !IS_SET(d->character->comm,COMM_NOCLAN) &&
             !IS_SET(d->character->comm,COMM_QUIET) )
        {
            act_new("$n clans '$t'",ch,argument,d->character,TO_VICT,POS_DEAD);
        }
    }

    return;
}

DO_FUNCTION(do_immtalk)
{
    if ( IS_NULLSTR(argument) )
    {
      if (IS_SET(ch->comm,COMM_NOWIZ))
      {
        send_to_char("Immortal channel is now ON\r\n",ch);
        REMOVE_BIT(ch->comm,COMM_NOWIZ);
      }
      else
      {
        send_to_char("Immortal channel is now OFF\r\n",ch);
        SET_BIT(ch->comm,COMM_NOWIZ);
      }
      return;
    }

    talk_channel(ch, argument, CHANNEL_WIZ);
    return;
}

/*
 * Generic channel function.
 */
void talk_channel( CHAR_DATA *ch, char *argument, int channel)
{
    char buf[MAX_STRING_LENGTH];
    DESCRIPTOR_DATA *d;
    int position;
    int old_con;
    int bit;

    if ( channel < 0 || channel > MAX_CHANNELS - 1 ) {
	return;
    }

    bit = channel_table[channel].bit;

    if ( IS_NULLSTR(argument) )
    {
        sprintf( buf, "What do you want to say on the %s channel?\r\n", 
	  channel_table[channel].name);
        buf[0] = UPPER(buf[0]);
        return;
    }

    if ( !IS_NPC(ch) && IS_SET(ch->comm, COMM_NOCHANNELS) )
    {
        send_to_char( "You can't talk on channels.\r\n", ch );
        return;
    }

    REMOVE_BIT(ch->comm, channel_table[channel].bit);


    sprintf(buf, "[%s%s{x] %s %s%s{x%s\r\n",
	channel_table[channel].color, channel_table[channel].name,
	ch->name,
	(*argument == ':') ? "" : "says, \"",
	(*argument == ':') ? argument + 1 : argument,
	(*argument == ':') ? "" : "\"");

    send_to_char(buf, ch);
    for ( d = descriptor_list; d != NULL; d = d->next )
    {
        CHAR_DATA *och;
        CHAR_DATA *vch;

	if ( !IS_PLAYING(d) )
	    continue;

        och = CHARACTER(d);
        vch = d->character;

        if ( vch != NULL && vch != ch
        &&  !IS_SET(och->comm, bit) )
        {
            if ( bit == COMM_NOSHOUT && IS_SET(och->comm, COMM_DEAF) )
                continue;

            if ( bit == COMM_NOWIZ && !IS_HERO(och) )
                continue;

            if (IS_SET(vch->comm, COMM_QUIET) )
                continue;

            if ( IS_WRITING(vch) )
                continue;

             position            = vch->position;
             old_con             = d->connected;
             d->connected        = CON_PLAYING;
             if ( bit != COMM_NOSHOUT  )
                 vch->position   = POS_STANDING;

	    send_to_char(buf, vch);
             vch->position       = position;
             d->connected        = old_con;
        }
    }

    return;
}
