#!/usr/local/bin/perl
use strict;
use warnings;

use IO::File;

my $ofh = IO::File->new("INC/gsn.h", "w");
if ( !$ofh ) {
  die "Cannot open INC/gsn.h: $!";
}

my $WHOAMI = `whoami`;
my $HOSTNAME= `hostname -f`;
my $DATE = `date`;

chop $WHOAMI;
chop $HOSTNAME;
chop $DATE;


print $ofh <<EOF;
/* Automatically generated
 * by ${WHOAMI}\@${HOSTNAME}
 * on ${DATE}
 */

#include "gsn-header.h"
EOF

my $ifh = IO::File->new("const.c", "r");
if ( !$ifh ) {
  die "Cannot open INC/gsn.h: $!";
}

while (<$ifh>) {
    last if /^const\s+struct\s+skill_type/o;
}


my $Table;
# Assuming that the skill table ends with a };
while (<$ifh>) {
    last if /^};/o;
    $Table .= $_;
}


# Remove all spaces
$Table =~ s/\s+/ /og;

# Remove all comments
# NOTE: this non-greedy match requires perl5
$Table =~ s#/\*.*?\*/##og;

# Now replace }, with a newline, so each skill is on its own line
$Table =~ s/},/\n/og;

my $sn = 0;
foreach my $a (split ('\n', $Table)) {
    if ($a =~ /\s+{\s+"([^"]+)"/o) {
        my $Skill = $1;
        $Skill =~ s/[- ]/_/g;
        print $ofh "GSN($Skill, $sn)\n";
        $sn++;
        
    }
}

$ifh->close;
$ofh->close;
