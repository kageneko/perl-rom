#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_BUFFERS	40
#define BUFFER_SIZE	1024*16
#define NEXT_BUFFER	buffers[(++current_buffer % MAX_BUFFERS)]

static char buffers[MAX_BUFFERS][BUFFER_SIZE];
static int current_buffer = 0;

char *get_buffer() {
  char *buffer = NEXT_BUFFER;
  *buffer = '\0';
  return buffer;
}

char *tprintf(char *format, ...) {
  char *buffer = get_buffer();

  va_list args;
  va_start(args, format);
  vsnprintf(buffer, BUFFER_SIZE, format, args);
  va_end(args);

  return buffer;
}
