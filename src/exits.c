#include "merc.h"

int find_door(CHAR_DATA *ch, char *arg);

const	sh_int	rev_dir[MAX_DIR] = {
    DIR_SOUTH, DIR_WEST, DIR_NORTH, DIR_EAST, DIR_DOWN, DIR_UP,
    DIR_SOUTHWEST, DIR_NORTHWEST, DIR_NORTHEAST, DIR_SOUTHEAST
};

const struct dir_type dir_table[MAX_DIR] = {
    {   "north",        "n",    DIR_NORTH       },
    {   "east",         "e",    DIR_EAST,       },
    {   "south",        "s",    DIR_SOUTH,      },
    {   "west",         "w",    DIR_WEST,       },
    {   "up",           "u",    DIR_UP,         },
    {   "down",         "d",    DIR_DOWN,       },
    {   "northeast",    "ne",   DIR_NORTHEAST,  },
    {   "southeast",    "se",   DIR_SOUTHEAST,  },
    {   "southwest",    "sw",   DIR_SOUTHWEST,  },
    {   "northwest",    "nw",   DIR_NORTHWEST,  }
};

int find_door( CHAR_DATA *ch, char *arg ) {
    EXIT_DATA *pexit;
    int door;

    for ( door = 0; door < MAX_DIR; ++door ) {
        if ( STR_CMP(arg, dir_table[door].long_name)
        ||   STR_CMP(arg, dir_table[door].short_name) ) {
            break;
        }
    }

    if ( door == MAX_DIR ) {
        for ( door = 0; door < MAX_DIR; ++door ) {
            if ( ( pexit = ch->in_room->exit[door] ) != NULL
            &&   IS_SET(pexit->exit_info, EX_ISDOOR)
            &&   pexit->keyword != NULL
            &&   is_name(arg, pexit->keyword) )
                return door;
        }
        act( "I see no $T here.", ch, NULL, arg, TO_CHAR );
        return -1;
    }

    if ( ( pexit = ch->in_room->exit[door] ) == NULL ) {
        act( "I see no door $T here.", ch, NULL, arg, TO_CHAR );
        return -1;
    }

    if ( !IS_SET(pexit->exit_info, EX_ISDOOR) ) {
        send_to_char( "You can't do that.\r\n", ch );
        return -1;
    }

    return door;
}


