#include "merc.h"

extern FILE *	fpArea;
extern char	strArea[MAX_INPUT_LENGTH];

/*
 * Append a string to a file.
 */
void append_file( CHAR_DATA *ch, char *file, char *str )
{
    FILE *fp;

    if ( IS_NPC(ch) || str[0] == '\0' )
	return;

    fclose( fpReserve );
    if ( ( fp = fopen( file, "a" ) ) == NULL )
    {
	perror( file );
	send_to_char( "Could not open the file!\r\n", ch );
    }
    else
    {
	fprintf( fp, "[%5d] %s: %s\n",
	    ch->in_room ? ch->in_room->vnum : 0, ch->name, str );
	fclose( fp );
    }

    fpReserve = fopen( NULL_FILE, "r" );
    return;
}



/*
 * Reports a bug.
 */
void bug( char *fmt, ... )
{
    char buf [4 * MAX_STRING_LENGTH];
    va_list args;
    va_start(args, fmt);

    if ( fpArea != NULL )
    {
	int iLine;
	int iChar;

	if ( fpArea == stdin ) {
	    iLine = 0;
	} else {
	    iChar = ftell( fpArea );
	    fseek( fpArea, 0, 0 );
	    for ( iLine = 0; ftell( fpArea ) < iChar; iLine++ ) {
		while ( getc( fpArea ) != '\n' )
		    ;
	    }
	    fseek( fpArea, iChar, 0 );
	}

	sprintf( buf, "[*****] FILE: %s LINE: %d", strArea, iLine );
	log_string( buf );
    }

    strcpy( buf, "[*****] BUG: " );
    vsnprintf( buf, 4 * MAX_STRING_LENGTH, fmt, args);
    va_end(args);
    log_string(buf);
    return;
}



/*
 * Writes a string to the log.
 */
void log_string( const char *str )
{
    char *strtime;

    strtime                    = ctime( &current_time );
    strtime[strlen(strtime)-1] = '\0';
    fprintf( stderr, "%s :: %s\n", strtime, str );
    return;
}

void log_stringf (char * fmt, ...) {
      char buf [2*MSL];
      va_list args;
      va_start (args, fmt);
      vsprintf (buf, fmt, args);
      va_end (args);
      log_string (buf);
}

