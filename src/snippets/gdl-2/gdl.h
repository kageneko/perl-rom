/* First param to RWFuns */
typedef enum {action_read, action_write, action_postinit} gdl_action; 

/* A read-write function type */
typedef void GDL_RWFun(gdl_action action, void *struct_ptr, void *member_ptr, const void *extra, FILE *fp);

typedef struct generic_disk_list /* Description of a structure member */
{
    const char *name;   /* What is the name of this variable? */
    int offset;         /* Compared to the head of the list, where is this variable? */
    GDL_RWFun* type;    /* What is the type of this variable? Pointer to a GDL_RWFun */
    const void *extra;  /* Extra data passed to the function */
    
} GDL;


#define BEGIN_EL_LIST(type) static const GDL * get_ ## type ## _gdl () {type *gdl_tmp_struct; static const GDL table [] = {
#define END_EL_LIST {NULL, 0,0, NULL} }; return table; }
#define EL(element,type,extra) { #element, (((char*)(&gdl_tmp_struct->element)) - ((char*)gdl_tmp_struct)), type, extra },

#define INT(x) EL(x,RWInteger,NULL)
#define SH_INT(x) EL(x,RWShortInt,NULL)
#define STR(x) EL(x,RWString,NULL)
#define FLAG(x,flag_type) EL(x,RWFlag,flag_type)
#define CUSTOM(x,fun,extra) EL(x,fun,extra)

#define LOAD_LIST(list_head,type,filename) (list_head) = generic_list_load (filename, get_ ## type ## _gdl(), sizeof(type))
#define SAVE_LIST(list_head,type,filename) generic_list_save (filename, get_ ## type ## _gdl(), list_head)

void* generic_list_load (const char *fname, const GDL *gdl, int size);
void  generic_list_save (const char *fname, const GDL *gdl, void *list);


/* R/W functions */

void RWInteger (gdl_action action, void *struct_ptr, void *member_ptr, const void *extra, FILE *fp);
void RWString (gdl_action action, void *struct_ptr, void *member_ptr, const void *extra, FILE *fp);
void RWFlag (gdl_action action, void *struct_ptr, void *member_ptr, const void *extra, FILE *fp);
void RWShortInt (gdl_action action, void *struct_ptr, void *member_ptr, const void *extra, FILE *fp);

/* These two are so I can keep the gdl.[ch] files on my ftp site and in my
 * MUD source syncronized. I use those functions to track used memory.
 */
 
#define add_mchunk(x) ((void)0)
#define del_mchunk(x) ((void)0)
