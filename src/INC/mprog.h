#ifndef __MPROG_H
#define __MPROG_H

#include "typedef.h"

/*
 * MOBprog definitions
 */                   
#define TRIG_ACT	(A)
#define TRIG_BRIBE	(B)
#define TRIG_DEATH	(C)
#define TRIG_ENTRY	(D)
#define TRIG_FIGHT	(E)
#define TRIG_GIVE	(F)
#define TRIG_GREET	(G)
#define TRIG_GRALL	(H)
#define TRIG_KILL	(I)
#define TRIG_HPCNT	(J)
#define TRIG_RANDOM	(K)
#define TRIG_SPEECH	(L)
#define TRIG_EXIT	(M)
#define TRIG_EXALL	(N)
#define TRIG_DELAY	(O)
#define TRIG_SURR	(P)

struct mprog_list {
    bool		valid;
    int			trig_type;
    char *		trig_phrase;
    sh_int		vnum;
    char *  		code;
    MPROG_LIST * 	next;
};

struct mprog_code {
    sh_int		vnum;
    char *		code;
    MPROG_CODE *	next;
};

#endif
