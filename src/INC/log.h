#ifndef __LOG_H
#define __LOG_H

#include "typedef.h"

void append_file( CHAR_DATA *ch, char *file, char *str );
void bug( char *fmt, ... );
void log_string( const char *str );
void log_stringf (char * fmt, ...);

#endif
