#ifndef __TYPEDEF_H
#define __TYPEDEF_H

#if     !defined(FALSE)
#define FALSE    0
#endif

#if     !defined(TRUE)
#define TRUE     1
#endif

typedef short   int                     sh_int;
typedef unsigned char                   bool;

typedef struct	kill_data		KILL_DATA;
typedef struct	mem_data		MEM_DATA;
typedef struct	mob_index_data		MOB_INDEX_DATA;
typedef struct	area_data		AREA_DATA;
typedef struct	char_data		CHAR_DATA;
typedef struct	pc_data			PC_DATA;
typedef struct	gen_data		GEN_DATA;
typedef struct	reset_data		RESET_DATA;
typedef	struct	exit_data		EXIT_DATA;
typedef struct	obj_data		OBJ_DATA;
typedef struct	room_index_data		ROOM_INDEX_DATA;
typedef struct	obj_index_data		OBJ_INDEX_DATA;
typedef struct	descriptor_data		DESCRIPTOR_DATA;
typedef	struct	affect_data		AFFECT_DATA;
typedef struct	note_data		NOTE_DATA;
typedef struct	buf_type		BUFFER;
typedef struct	shop_data		SHOP_DATA;
typedef struct	mprog_list		MPROG_LIST;
typedef struct	mprog_code		MPROG_CODE;

typedef void DO_FUN		(CHAR_DATA *ch, char *argument);

typedef bool MOB_SPEC_FUN	(CHAR_DATA *ch);
typedef bool OBJ_SPEC_FUN	(OBJ_DATA *obj);
typedef bool ROOM_SPEC_FUN	(ROOM_INDEX_DATA *room);
typedef void SPELL_FUN		(int sn, int level, CHAR_DATA *ch, void *vo,
	int target);

#endif
