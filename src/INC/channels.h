#ifndef __CHANNELS_H
#define __CHANNELS_H

enum {
  CHANNEL_WIZ = 0,
  CHANNEL_AUCTION,
  CHANNEL_GOSSIP,
  CHANNEL_QUESTION,
  CHANNEL_MUSIC,
  CHANNEL_CLAN,
  CHANNEL_QUOTE,
  CHANNEL_GRATS,
  CHANNEL_SHOUTS,
  MAX_CHANNELS
};


struct channel_type {
  char *name;
  char *color;
  int bit;
};

extern const struct channel_type	channel_table[];
void talk_channel( CHAR_DATA *ch, char *argument, int channel);

#endif
