/*
 * Utility macros.
 */
#define IS_VALID(data)		((data) != NULL && (data)->valid)
#define VALIDATE(data)		((data)->valid = TRUE)
#define INVALIDATE(data)	((data)->valid = FALSE)
#define UMIN(a, b)		((a) < (b) ? (a) : (b))
#define UMAX(a, b)		((a) > (b) ? (a) : (b))
#define URANGE(a, b, c)		((b) < (a) ? (a) : ((b) > (c) ? (c) : (b)))
#define LOWER(c)		( tolower(c) )
#define UPPER(c)		( toupper(c) )
#define IS_SET(flag, bit)	((flag) & (bit))
#define SET_BIT(var, bit)	((var) |= (bit))
#define REMOVE_BIT(var, bit)	((var) &= ~(bit))

#define IS_NULLSTR(str)		( ((str) == NULL) || (*(str) == '\0') )
#define STR_CMP(a,b)		( (LOWER(*(a)) == LOWER(*(b))) && !str_cmp((a),(b)) )
#define STR_PREFIX(a,b)		( (LOWER(*(a)) == LOWER(*(b))) && !str_prefix((a),(b)) )
#define IS_WRITING(ch)          ( (ch) && (ch)->desc && (ch)->desc->pString )
#define replace_string(old,new)	\
	{ free_string(old); old = str_dup(new); }
#define array_size(array)	( sizeof(array) / sizeof((array)[0]) )

#define	MAX_STRING_BUFFERS	30
#define MAX_BUFFER_SIZE		(MAX_STRING_LENGTH * 4)
#define	STRING_BUFFER		\
	(string_buffer[++curr_string_buffer % MAX_STRING_BUFFERS])
extern	int	curr_string_buffer;
extern	char	string_buffer[MAX_STRING_BUFFERS][MAX_BUFFER_SIZE];


/*
 * Character macros.
 */
#define IS_NPC(ch)		(IS_SET((ch)->act, ACT_IS_NPC))
#define IS_IMMORTAL(ch)		(get_trust(ch) >= LEVEL_IMMORTAL)
#define IS_HERO(ch)		(get_trust(ch) >= LEVEL_HERO)

#define IS_TRUSTED(ch,level)	(get_trust((ch)) >= (level))
#define IS_AFFECTED(ch, sn)	(IS_SET((ch)->affected_by, (sn)))

#define GET_AGE(ch)		((int) (17 + ((ch)->played \
				    + current_time - (ch)->logon )/72000))

#define IS_GOOD(ch)		(ch->alignment >= 350)
#define IS_EVIL(ch)		(ch->alignment <= -350)
#define IS_NEUTRAL(ch)		(!IS_GOOD(ch) && !IS_EVIL(ch))

#define IS_AWAKE(ch)		(ch->position > POS_SLEEPING)
#define GET_AC(ch,type)		((ch)->armor[type]			    \
		        + ( IS_AWAKE(ch)			    \
			? dex_app[get_curr_stat(ch,STAT_DEX)].defensive : 0 ))  
#define GET_HITROLL(ch)	\
		((ch)->hitroll+str_app[get_curr_stat(ch,STAT_STR)].tohit)
#define GET_DAMROLL(ch) \
		((ch)->damroll+str_app[get_curr_stat(ch,STAT_STR)].todam)

#define IS_OUTSIDE(ch)		(!IS_SET(				    \
				    (ch)->in_room->room_flags,		    \
				    ROOM_INDOORS))

#define WAIT_STATE(ch, npulse)	((ch)->wait = UMAX((ch)->wait, (npulse)))
#define DAZE_STATE(ch, npulse)  ((ch)->daze = UMAX((ch)->daze, (npulse)))
#define get_carry_weight(ch)	((ch)->carry_weight + (ch)->silver/10 +  \
						      (ch)->gold * 2 / 5)

#define act(format,ch,arg1,arg2,type)\
	act_new((format),(ch),(arg1),(arg2),(type),POS_RESTING)
#define	CHARACTER(d)		(d)->original ? (d)->original : (d)->character
#define IS_PLAYING(d)		( (d)->connected == CON_PLAYING )
#define IS_QUESTOR(ch)		( IS_SET((ch)->act, PLR_QUESTOR) )
#define HAS_TRIGGER(ch,trig)	(IS_NPC(ch) && IS_SET((ch)->pIndexData->mprog_flags,(trig)))

/*
 * Object macros.
 */
#define CAN_WEAR(obj, part)	(IS_SET((obj)->wear_flags,  (part)))
#define IS_OBJ_STAT(obj, stat)	(IS_SET((obj)->extra_flags, (stat)))
#define IS_WEAPON_STAT(obj,stat)(IS_SET((obj)->value[4],(stat)))
#define WEIGHT_MULT(obj)	((obj)->item_type == ITEM_CONTAINER ? \
	(obj)->value[4] : 100)



/*
 * Description macros.
 */
#define PERS(ch, looker)	( can_see((looker), (ch)) ? \
				( IS_NPC(ch) ? (ch)->short_descr : \
				  (ch)->name ) : "someone" )
