#ifndef __AREA_H
#define __AREA_H

#include "typedef.h"

/* New area_data for OLC */
struct	area_data {
    AREA_DATA *		next;		/*
    RESET_DATA *	reset_first;     *  Removed for OLC reset system.
    RESET_DATA *	reset_last;	 */
    char *		name;
    char *		credits;
    int 		age;
    int 		nplayer;
    bool                empty;          /* ROM OLC */
    char *		filename;	/* OLC */
    char *		builders;	/* OLC */ /* Listing of */
    int			security;	/* OLC */ /* Value 1-9  */
    int			lvnum;		/* OLC */ /* Lower vnum */
    int			uvnum;		/* OLC */ /* Upper vnum */
    int			vnum;		/* OLC */ /* Area vnum  */
    int			area_flags;	/* OLC */
};

#endif
