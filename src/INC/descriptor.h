#ifndef __DESCRIPTOR_H
#define __DESCRIPTOR_H

#include "typedef.h"

/*
 * Connected state for a channel.
 */
enum {
  CON_PLAYING = 0,
  CON_GET_NAME,
  CON_GET_OLD_PASSWORD,
  CON_CONFIRM_NEW_NAME,
  CON_GET_NEW_PASSWORD,
  CON_CONFIRM_NEW_PASSWORD,
  CON_GET_NEW_RACE,
  CON_GET_NEW_SEX,
  CON_GET_NEW_CLASS,
  CON_GET_ALIGNMENT,
  CON_DEFAULT_CHOICE,
  CON_GEN_GROUPS,
  CON_PICK_WEAPON,
  CON_READ_IMOTD,
  CON_READ_MOTD,
  CON_BREAK_CONNECT,
  CON_COPYOVER_RECOVER,
  MAX_CON
};

extern const char *	con_names[MAX_CON];

/*
 * Descriptor (channel) structure.
 */
struct	descriptor_data
{
    DESCRIPTOR_DATA *	next;
    DESCRIPTOR_DATA *	snoop_by;
    CHAR_DATA *		character;
    CHAR_DATA *		original;
    bool		valid;
    char *		host;
    sh_int		descriptor;
    sh_int		connected;
    bool		fcommand;
    char		inbuf		[4 * MAX_INPUT_LENGTH];
    char		incomm		[MAX_INPUT_LENGTH];
    char		inlast		[MAX_INPUT_LENGTH];
    int			repeat;
    char *		outbuf;
    int			outsize;
    int			outtop;
    char *		showstr_head;
    char *		showstr_point;
    void *              pEdit;		/* OLC */
    char **             pString;	/* OLC */
    int			editor;		/* OLC */
};

#endif
