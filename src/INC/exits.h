#ifndef __EXITS_H
#define __EXITS_H

#include "typedef.h"

/*
 * Directions.
 * Used in #ROOMS.
 */
enum { DIR_NORTH = 0, DIR_EAST, DIR_SOUTH, DIR_WEST, DIR_UP, DIR_DOWN,
  DIR_NORTHEAST, DIR_SOUTHEAST, DIR_SOUTHWEST, DIR_NORTHWEST, MAX_DIR };

struct dir_type {
  char *long_name;
  char *short_name;
  int dir;
};

/*
 * Exit data.
 */
struct  exit_data
{
    EXIT_DATA *         next;           /* OLC */
    int                 rs_flags;       /* OLC */
    int                 orig_door;      /* OLC */

    union
    {
        ROOM_INDEX_DATA *       to_room;
        sh_int                  vnum;
    } u1;
    sh_int              exit_info;
    sh_int              key;
    char *              keyword;
    char *              description;
};


extern const struct dir_type	dir_table[MAX_DIR];
extern const sh_int		rev_dir[MAX_DIR];

int find_door( CHAR_DATA *ch, char *arg );

#endif
