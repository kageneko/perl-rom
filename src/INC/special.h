#ifndef __SPECIAL_H
#define __SPECIAL_H

#define DECLARE_MOB_SPEC_FUN( fun )	MOB_SPEC_FUN fun
#define DECLARE_OBJ_SPEC_FUN( fun )	OBJ_SPEC_FUN fun
#define DECLARE_ROOM_SPEC_FUN( fun )	OBJ_SPEC_FUN fun

#define	DO_FUNCTION(func)	void func(CHAR_DATA *ch, char *argument)
#define	MOB_SPEC_FUNCTION(func)	bool func(CHAR_DATA *ch)
#define	OBJ_SPEC_FUNCTION(func)	bool func(OBJ_DATA *obj)
#define	ROOM_SPEC_FUNCTION(func)	bool func(ROOM_INDEX_DATA *room)
#define	SPELL_FUNCTION(func)	\
void func(int sn, int level, CHAR_DATA *ch, void *vo, int target)

struct mob_spec_type
{
    char *              name;                   /* special function name */
    MOB_SPEC_FUN *      function;               /* the function */
};

#endif
