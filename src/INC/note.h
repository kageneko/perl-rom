#ifndef __NOTE_H
#define __NOTE_H

/*
 * Data structure for notes.
 */

enum {
  NOTE_NOTE = 0, NOTE_IDEA, NOTE_PENALTY, NOTE_NEWS, NOTE_CHANGES
};

struct	note_data {
    NOTE_DATA *	next;
    bool 	valid;
    sh_int	type;
    char *	sender;
    char *	date;
    char *	to_list;
    char *	subject;
    char *	text;
    time_t  	date_stamp;
};

#endif
