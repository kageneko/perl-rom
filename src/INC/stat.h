#ifndef __STAT_H
#define __STAT_H

#include "typedef.h"

enum {
  STAT_STR = 0, STAT_INT, STAT_WIS, STAT_DEX, STAT_CON,
  MAX_STATS
};

struct ability_type {
   char *	name;
   char *	abbrev;
};

/*
 * Attribute bonus structures.
 */
struct	str_app_type {
    sh_int	tohit;
    sh_int	todam;
    sh_int	carry;
    sh_int	wield;
};

struct	int_app_type {
    sh_int	learn;
};

struct	wis_app_type {
    sh_int	practice;
};

struct	dex_app_type {
    sh_int	defensive;
};

struct	con_app_type {
    sh_int	hitp;
    sh_int	shock;
};

#endif
