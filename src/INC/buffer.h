#ifndef __BUFFER_H
#define __BUFFER_H

#include "typedef.h"

struct buf_type {
    BUFFER *    next;
    bool        valid;
    sh_int      state;  /* error state of the buffer */
    sh_int      size;   /* size in k */
    char *      string; /* buffer's string */
};

#define	MAX_STRING_BUFFERS	30
#define MAX_BUFFER_SIZE		(MAX_STRING_LENGTH * 4)
#define	STRING_BUFFER		\
	(string_buffer[++curr_string_buffer % MAX_STRING_BUFFERS])
extern	int	curr_string_buffer;
extern	char	string_buffer[MAX_STRING_BUFFERS][MAX_BUFFER_SIZE];

#endif
