#ifndef __DEFINE_H
#define __DEFINE_H

#include "typedef.h"

#define A		  	1
#define B			2
#define C			4
#define D			8
#define E			16
#define F			32
#define G			64
#define H			128

#define I			256
#define J			512
#define K		        1024
#define L		 	2048
#define M			4096
#define N		 	8192
#define O			16384
#define P			32768

#define Q			65536
#define R			131072
#define S			262144
#define T			524288
#define U			1048576
#define V			2097152
#define W			4194304
#define X			8388608

#define Y			16777216
#define Z			33554432
#define aa			67108864 	/* doubled due to conflicts */
#define bb			134217728
#define cc			268435456    
#define dd			536870912
#define ee			1073741824

#define args( list )			list
#define DECLARE_DO_FUN( fun )		DO_FUN fun
#define DECLARE_SPELL_FUN( fun )	SPELL_FUN fun

/*
 * String and memory management parameters.
 */
#define	MAX_KEY_HASH		 1024
#define MAX_STRING_LENGTH	 4608
#define MAX_INPUT_LENGTH	  256
#define PAGELEN			   22

/* I am lazy :) */
#define MSL MAX_STRING_LENGTH
#define MIL MAX_INPUT_LENGTH

/*
 * Game parameters.
 * Increase the max'es if you add more of something.
 * Adjust the pulse numbers to suit yourself.
 */
#define MAX_SOCIALS		  256
#define MAX_SKILL		  150
#define MAX_GROUP		   30
#define MAX_IN_GROUP		   15
#define MAX_ALIAS		    5
#define MAX_CLASS		    4
#define MAX_PC_RACE		    5
#define MAX_CLAN		    3
#define MAX_DAMAGE_MESSAGE	   42
#define MAX_LEVEL		   60
#define LEVEL_HERO		   (MAX_LEVEL - 9)
#define LEVEL_IMMORTAL		   (MAX_LEVEL - 8)

#define PULSE_PER_SECOND	    4
#define PULSE_VIOLENCE		  ( 3 * PULSE_PER_SECOND)
#define PULSE_MOBILE		  ( 4 * PULSE_PER_SECOND)
#define PULSE_MUSIC		  ( 6 * PULSE_PER_SECOND)
#define PULSE_TICK		  (60 * PULSE_PER_SECOND)
#define PULSE_AREA		  (120 * PULSE_PER_SECOND)

#define IDLE_TIMEOUT		20

#define IMPLEMENTOR		MAX_LEVEL
#define	CREATOR			(MAX_LEVEL - 1)
#define SUPREME			(MAX_LEVEL - 2)
#define DEITY			(MAX_LEVEL - 3)
#define GOD			(MAX_LEVEL - 4)
#define IMMORTAL		(MAX_LEVEL - 5)
#define DEMI			(MAX_LEVEL - 6)
#define ANGEL			(MAX_LEVEL - 7)
#define AVATAR			(MAX_LEVEL - 8)
#define HERO			LEVEL_HERO

/*
 * Colour stuff by Lope of Loping Through The MUD
 */
#define CLEAR		"[0m"		/* Resets Colour	*/
#define C_BLACK		"[0;30m"  	
#define C_RED		"[0;31m"	/* Normal Colours	*/
#define C_GREEN		"[0;32m"
#define C_YELLOW	"[0;33m"
#define C_BLUE		"[0;34m"
#define C_MAGENTA	"[0;35m"
#define C_CYAN		"[0;36m"
#define C_WHITE		"[0;37m"
#define C_D_GREY	"[1;30m"  	/* Light Colors		*/
#define C_B_RED		"[1;31m"
#define C_B_GREEN	"[1;32m"
#define C_B_YELLOW	"[1;33m"
#define C_B_BLUE	"[1;34m"
#define C_B_MAGENTA	"[1;35m"
#define C_B_CYAN	"[1;36m"
#define C_B_WHITE	"[1;37m"

/*
 * TO types for act.
 */
enum { TO_ROOM = 0, TO_NOTVICT, TO_VICT, TO_CHAR, TO_ALL, TO_GROUP,
  TO_CHARVICT, MAX_TO };

#define MAX_GUILD 	2

#define EFFECT_FUNCTION(func)	\
void func(void *vo, int level, int dam, int target)

#define	DO_FUNCTION(func)	void func(CHAR_DATA *ch, char *argument)

#define	SPELL_FUNCTION(func)	\
void func(int sn, int level, CHAR_DATA *ch, void *vo, int target)

#endif
