#ifndef __AFFECT_H
#define __AFFECT_H

#include "typedef.h"

enum {
  TO_AFFECTS = 0, TO_OBJECT, TO_IMMUNE, TO_RESIST, TO_VULN, TO_WEAPON
};

/*
 * An affect.
 */
struct	affect_data
{
    AFFECT_DATA *	next;
    bool		valid;
    sh_int		where;
    sh_int		type;
    sh_int		level;
    sh_int		duration;
    sh_int		location;
    sh_int		modifier;
    int			bitvector;
};

#endif
