#!perl
use 5.006;
use strict;
use warnings FATAL => 'all';
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Game::Mud::ROM' ) || print "Bail out!\n";
}

diag( "Testing Game::Mud::ROM $Game::Mud::ROM::VERSION, Perl $], $^X" );
