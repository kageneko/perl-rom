package Game::Mud::ROM::WithMudState;
use Modern::Perl;
use Moose::Role;
use namespace::sweep;

use Game::Mud::ROM::MudState::TimeInfo;
use Game::Mud::ROM::MudState::WeatherInfo;

my $STATE = {};

has mud_state => (is => 'ro', isa => 'HashRef', builder => '_init_state');

sub _init_state {
  $STATE = {
    pulse_per_second => 4,
    running => 1,
    current_time => time,
    time_info => Game::Mud::ROM::MudState::TimeInfo->new,
    weather_info => Game::Mud::ROM::MudState::WeatherInfo->new,
  };

  return $STATE;
}

1;
