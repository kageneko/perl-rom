package Game::Mud::ROM::DB::Thing;
use Modern::Perl;
use Moose;
use namespace::sweep;

sub is {
  my ($self, $type) = @_;

  my $package = "Game::Mud::ROM::DB::${type}";
  return $self->isa($package);
}

__PACKAGE__->meta->make_immutable;

1;
