package Game::Mud::ROM::DB::Character;
use Modern::Perl;
use Moose;
use namespace::sweep;

use Game::Mud::ROM::DB::Thing;
use Game::Mud::ROM::DB::Room;

extends 'Game::Mud::ROM::DB::Thing';

has in_room => (is => 'rw', isa => 'Maybe[Game::Mud::ROM::DB::Room]');
has was_in_room => (is => 'rw', isa => 'Maybe[Game::Mud::ROM::DB::Room]');

sub is_player {
  return 0;
}

__PACKAGE__->meta->make_immutable;

1;
