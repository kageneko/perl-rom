package Game::Mud::ROM::DB::Room;
use Modern::Perl;
use Moose;
use namespace::sweep;

use Game::Mud::ROM::DB::Thing;

extends 'Game::Mud::ROM::DB::Thing';

__PACKAGE__->meta->make_immutable;

1;
