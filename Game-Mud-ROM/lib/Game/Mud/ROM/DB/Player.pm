package Game::Mud::ROM::DB::Player;
use Modern::Perl;
use Moose;
use namespace::sweep;

extends 'Game::Mud::ROM::DB::Character';

use Game::Mud::ROM::DB::Room;

has client => (is => 'rw', isa => 'Maybe[Game::Mud::ROM::Net::Client]');

use constant ROOM_VNUM_LIMBO => 1;
use constant TO_ROOM => 1;
sub stop_idling {
  my $self = shift;

  if ( !$self->client->is_playing || !$self->was_in_room || ($self->in_room->vnum != ROOM_VNUM_LIMBO) ) {
    return;
  }

  $self->timer(0);
  $self->from_room;
  $self->to_room($self->was_in_room);
  $self->was_in_room(undef);
  act('$n has returned from the void.', $self, undef, undef, TO_ROOM);
  return;
}

sub is_player {
  return 1;
}

__PACKAGE__->meta->make_immutable;

1;
