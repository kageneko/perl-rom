package Game::Mud::ROM::Text;
use Modern::Perl;
use Moose;
use namespace::sweep;

use IO::Dir;
use File::Slurp;

use autobox;
use autobox::Core STRING => ['Game::Mud::ROM::Util::Autobox::'];

has texts => (is => 'ro', isa => 'HashRef', default => sub { {} });

with 'Game::Mud::ROM::WithLogger';

sub BUILD {
  my ($self, $args) = @_;

  my $dir = IO::Dir->new('text');
  if ( !defined $dir ) {
    $self->logger->error("Unable to open text director: $!");
  } else {
    while ( defined($_ = $dir->read) ) {
      next if /^\..*/o;
      my $keyword = "text." . $_->lc;
      $keyword =~ s/.te?xt$//go;
      my $text = File::Slurp::read_file("text/$_");
      my $first = $keyword->substr(0, 1);
      $self->texts->{$first}->{$keyword} = $text;
    }
  }
}

sub load {
  my ($class) = @_;
}

sub add_text {
  my ($self, $keywords, $text) = @_;

  if ( !ref($keywords) ) {
    $keywords = [ $keywords ];
  }

  foreach my $keyword ( @$keywords ) {
    my $first = $keyword->substr(0, 1)->lc;
    $self->texts->{$first}->{$keyword} = $text;
  }
}

sub get_text {
  my ($self, $keyword) = @_;

  $keyword = $keyword->lc;
  my $first = $keyword->substr(0, 1);
  my $text = $self->texts->{$first}->{$keyword};
  return $text if defined $text;
  my $hash = $self->texts->{$first};
  return '' if !defined $hash;

  foreach my $key ( $hash->keys ) {
    if ( $key->starts_with($keyword) ) {
      return $hash->{$key};
    }
  }
  return '';
}

__PACKAGE__->meta->make_immutable;

1;
