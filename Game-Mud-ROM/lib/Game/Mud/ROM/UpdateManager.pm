package Game::Mud::ROM::UpdateManager;
use Modern::Perl;
use Moose;
use namespace::sweep;

has handlers => (is => 'ro', isa => 'ArrayRef', default => sub { [] });

sub BUILD {
  my ($self, $args) = @_;
}

sub run {
  my $self = shift;

  foreach my $handler ( $self->handlers ) {
    $handler->update;
  }
}

__PACKAGE__->meta->make_immutable;

1;
