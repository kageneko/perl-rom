package Game::Mud::ROM::Net::State::Playing;
use Modern::Perl;
use Moose;
use namespace::sweep;

extends 'Game::Mud::ROM::Net::State';

sub run {
  my ($client, $input) = @_;

  if ( !run_olc_editor($client) ) {
    substitute_alias($client, $client->incomm);
  }
}

sub run_olc_editor {
}

sub substitute_alias {
  my ($client, $argument) = @_;

  my $character = $client->original_character;
  if ( $character->prefix && !$argument->starts_with('prefix') ) {
    $argument = $character->prefix . " " . $argument;
  }

  if ( !$character->isa('Player') || $character->aliases->keys == 0
  || $argument->starts_with('alias') || $argument->starts_with('una')
  || $argument->starts_with('prefix') ) {
    $client->interpret($client->character, $argument);
    return;
  }

  my ($command, $arguments) = $argument->split(/\s+/o, 2);
  $character->aliases->each(sub {
    my ($key, $value) = @_;
    if ( lc $command eq lc $key ) {
      $argument = "$value $arguments";
      last;
    }
  });
  logger->info("Command: $command");
  interpret($client->character, $command);
}

__PACKAGE__->meta->make_immutable;

1;

