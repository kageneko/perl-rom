package Game::Mud::ROM::Net::State;
use Modern::Perl;
use Moose;
use namespace::sweep;

with 'Game::Mud::ROM::WithLogger';

sub run {
  my $self = shift;
  $self->logger->error("Unimplemented run for state " . ref($self));
}

__PACKAGE__->meta->make_immutable;

1;
