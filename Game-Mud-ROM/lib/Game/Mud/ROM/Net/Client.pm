package Game::Mud::ROM::Net::Client;
use Modern::Perl;
use Moose;
use namespace::sweep;

use Game::Mud::ROM::Util::Autobox::STRING;

use autobox;
use autobox::Core STRING => ['Game::Mud::ROM::Util::Autobox::'];

use IO::Socket::INET;
use Module::Load ();
use POSIX;

use Game::Mud::ROM::DB::Character;
use Game::Mud::ROM::Net::State;

has socket => (is => 'rw', isa => 'IO::Socket::INET');
has state => (is => 'rw', isa => 'Game::Mud::ROM::Net::State');

has snoop_by => (is => 'rw', isa => 'Maybe[Game::Mud::ROM::DB::Character]');
has character => (is => 'rw', isa => 'Maybe[Game::Mud::ROM::DB::Character]');
has original => (is => 'rw', isa => 'Maybe[Game::Mud::ROM::DB::Character]');

has valid => (is => 'rw', isa => 'Bool', default => 0);
has host => (is => 'rw', isa => 'Str', default => '(unknown)');
has fcommand => (is => 'rw', isa => 'Bool', default => 0);

has inbuf => (is => 'rw', isa => 'Str', default => '');
has incomm => (is => 'rw', isa => 'Str', default => '');
has inlast => (is => 'rw', isa => 'Str', default => '');
has repeat => (is => 'rw', isa => 'Int', default => 0);
has outbuf => (is => 'rw', isa => 'Str', default => '');

with 'Game::Mud::ROM::WithLogger', 'Game::Mud::ROM::WithMudState';

use constant DEFAULT_STATE => 'GetName';

sub BUILD {
  my ($self, $args) = @_;
  my $socket = $self->socket;
  $self->change_state(DEFAULT_STATE);
}

my %STATES = ();
sub change_state {
  my ($self, $state) = @_;
  if ( !$state ) {
    return;
  }

  my $object = $STATES{$state};
  if ( !defined $object ) {
    my $class = "Game::Mud::ROM::Net::State::${state}";
    Module::Load::load($class);
    $STATES{$state} = $object = $class->new();
  }
  $self->state($object);
}

sub is_state {
  my ($self, $state) = @_;
  my $re = "${state}\$";
  my $object = $self->state;
  my $ref = ref($object);
  return $ref && $ref =~ /$re/;
}

sub is_playing {
  my ($self) = @_;
  return $self->character && ref($self->state) =~ /Playing$/o;
}

sub original_character {
  my $self = shift;
  return $self->{original} ? $self->{original} : $self->{character};
}

sub close {
  my ($self) = @_;
  my $socket = $self->socket;
  $socket->close;
}

sub read_from_descriptor {
  my $self = shift;

  my $iStart = 0;
  if ( $self->incomm->length > 0 ) {
    return 1;
  }

  my $buffer = '';
  my $nRead = 0;
  while (1) {
    my $tmp = 0;
    $nRead = $self->socket->read($tmp, 1024);
    if ( $nRead > 0 ) {
      $buffer .= $tmp;
      my $c = $tmp->substr(-1, 1);
      if ( $c eq "\r" || $c eq "\n" ) {
        last;
      } elsif ( $nRead == 0 ) {
        $self->logger->warn("EOF encountered on read on " . $self->socket->fileno);
        return;
      } elsif ( $! == EWOULDBLOCK ) {
        last;
      } else {
        $self->logger->error("read_from_descriptor: $!");
        return;
      }
    }
  }

  $self->inbuf($buffer);

  return 1;
}

sub write_to_descriptor {
  my ($self, $text) = @_;

  if ( !defined $text || $text->length == 0 ) {
    return 1;
  }

  $text =~ s/\r\n?/\n/go;
  my $count = $self->socket->send($text);
  if ( $count < 0 ) {
    $self->logger->error(sprintf("write_to_descriptor #%d: %s", $self->socket->fileno, $!));
    return;
  }
  $self->logger->debug("Sent $count bytes to $self");

  return 1;
}

sub read_from_buffer {
  my ($self) = @_;
  my ($i, $j, $k);

  if ( $self->incomm->length > 0 ) {
    return;
  }

  $i = $self->inbuf->index_of(qw/[\r\n]/);
  if ( $i == -1 ) {
    return;
  }

  my $input = $self->inbuf->substr(0, $i);
  $input =~ s/[\r\n]+$//go;
  $self->incomm($input);

  my $buffer = $self->inbuf->substr($i);
  $buffer =~ s/^[\r\n]+//go;
  $self->inbuf($buffer);

  $self->logger->info("$input ==> $buffer");
  if ( $input->substr(0, 1) eq '!' ) {
    $self->incomm($self->inlast);
  } else {
    $self->inlast($input);
  }

  return;
}

sub write_to_buffer {
  my ($self, $text) = @_;

  if ( !defined $text || $text->length == 0 ) {
    return;
  }

  my $buffer = $self->outbuf;
  if ( $buffer->length == 0 ) {
    $buffer = "\r\n";
  }

  $buffer .= $text;
  $self->outbuf($buffer);
}

sub process_output {
  my ($self, $prompt) = @_;

  if ( $self->mud_state->{running} ) {
    if ( $prompt && $self->is_state('Playing') ) {
      if ( $self->p_string ) {
        $self->write_to_buffer("> ");
      } else {
        my $character = $self->character;
        my $victim = $character->fighting;
        if ( defined($victim) && $character->can_see($victim) ) {
          my $msg = sprintf("%s %s \n",
            $victim->is_player ? $victim->name : $victim->short_descr,
            $victim->hp->message)->ucfirst;
          $self->write_to_buffer(convert_color($msg));
        }

        $character = $self->original_character;
        if ( !$character->comm_flags->get('Compact') ) {
          $self->write_to_buffer("\r\n");
        }

        if ( !$character->comm_flags->get('Prompt') ) {
          $self->bust_a_prompt();
        }

        if ( !$character->comm_flags->get('Telnet_GA') ) {
          $self->write_to_buffer($self->telnet_ga);
        }
      }
    } 
  }

  if ( $self->outbuf->length == 0 ) {
    return 1;
  }

  if ( $self->snoop_by ) {
    if ( $self->character ) {
      $self->snoop_by->write_to_buffer($self->character->name);
    }
    $self->snoop_by->write_to_buffer('> ');
    $self->snoop_by->write_to_buffer($self->outbuf);
  }
  
  if ( !$self->write_to_descriptor($self->outbuf) ) {
    $self->outbuf('');
    return;
  } else {
    $self->outbuf('');
    return 1;
  }
}

sub bust_a_prompt {
  my $self = shift;
  my $character = $self->original_character;
}

sub convert_color {
  my ($self, $character, $string) = @_;

  if ( !defined $character || !$character->is_player || !defined $string || $string->length == 0 ) {
    return $string;
  }

  if ( $character->act_flags->get('Color') ) {
    $string = $string->convert_color;
  } else {
    $string = $string->strip_color;
  }

  return $string;
}

__PACKAGE__->meta->make_immutable;

1;
