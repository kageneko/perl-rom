package Game::Mud::ROM::WithLogger;
use Modern::Perl;
use Moose::Role;
use namespace::sweep;

use Log::Log4perl;
use File::Spec;

has logger => (is => 'ro', isa => 'Item', builder => 'get_logger');

my $INIT_LOGGER = 0;
sub init_logger {
  my $self = shift;
  
  Log::Log4perl::init(File::Spec->catfile('conf', 'log4perl.conf'));
  $INIT_LOGGER = 1;
}

sub get_logger {
  my ($self) = @_;
  if ( !$INIT_LOGGER ) {
  	$self->init_logger;
  }
  return Log::Log4perl->get_logger(ref($self));
}

1;
