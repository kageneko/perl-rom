package Game::Mud::ROM::Updater::ViolenceUpdater;
use Modern::Perl;
use Moose;
use namespace::sweep;

with 'Game::Mud::ROM::WithMudState', 'Game::Mud::ROM::Updater';

sub init {
  my $self = shift;

  $self->max_pulses(3 * $self->mud_state->{pulse_per_second});
}

sub run {
  my $self = shift;
}

__PACKAGE__->meta->make_immutable;

1;
