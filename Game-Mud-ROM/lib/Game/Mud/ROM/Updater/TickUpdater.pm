package Game::Mud::ROM::Updater::TickUpdater;
use Modern::Perl;
use Moose;
use namespace::sweep;

use List::Util qw/ max min /;

with 'Game::Mud::ROM::WithMudState', 'Game::Mud::ROM::Updater';

use Game::Mud::ROM::Util::Dice;

sub init {
  my $self = shift;

  $self->max_pulses(60 * $self->mud_state->{pulse_per_second});
}

sub run {
  my $self = shift;

  $self->weather_update;
  $self->character_update;
  $self->object_update;
}

sub weather_update {
  my $self = shift;

  my $msg = '';

  my $time_info = $self->mud_state->{time_info};
  $time_info->advance_hour;
  my $hour = $time_info->hour;
  my $weather_info = $self->mud_state->{weather_info};

  if ( $hour == 5 ) {
	$weather_info->dawn;
	$msg = "The day has begun.\n";
  } elsif ( $hour == 6 ) {
    $weather_info->day;
    $msg = "The sun rises in the east.\n";  	
  } elsif ( $hour == 19 ) {
    $weather_info->dusk;
    $msg = "The sun slowly disappears in the west.\n";
  } elsif ( $hour == 20 ) {
    $weather_info->night;
    $msg = "The night has begun.\n";
  }

  my $diff = 0;
  if ( $time_info->month >= 9 && $time_info->month <= 16 ) {
    $diff = $weather_info->mmhg > 985 ? -2 : 2;
  } else {
    $diff = $weather_info->mmhg > 1015 ? -2 : 2;
  }

  my $change = $weather_info->change;
  $change += $diff * roll_dice(1, 4) + roll_dice(2, 6) - roll_dice(2, 6);
  $change = min(max($change, -12), 12);
  $weather_info->change($change);

  my $mmhg = $weather_info->mmhg;
  $mmhg = min(max($mmhg + $change, 960), 1040);
  $weather_info->mmhg($mmhg);

  if ( $weather_info->cloudless ) {
    if ( $mmhg < 990 || ($mmhg < 1010 && number_bits(2) == 0) ) {
      $msg .= "The sky is getting cloudy.\n";
      $weather_info->cloudy;
    }
  } elsif ( $weather_info->cloudy ) {
    if ( $mmhg < 970 || ($mmhg < 990 && number_bits(2) == 0) ) {
      $msg .= "It starts to rain.\n";
      $weather_info->raining;
    }

    if ( $mmhg > 1030 && number_bits(2) == 0 ) {
      $msg .= "It clouds disappear.\n";
      $weather_info->cloudless;
    }
  } elsif ( $weather_info->raining ) {
    if ( $mmhg < 970 && number_bits(2) == 0 ) {
      $msg .= "Lightning flashes in the sky.\n";
      $weather_info->lightning;
    }

    if ( $mmhg > 1030 || ($mmhg > 1010 && number_bits(2) == 0) ) {
      $msg .= "The rain stopped.\n";
      $weather_info->cloudy;
    }
  } elsif ( $weather_info->lightning ) {
    if ( $mmhg > 1010 || ($mmhg > 990 && number_bits(2) == 0) ) {
      $msg .= "The lightning has stopped.\n";
      $weather_info->raining;
    }
  } else {
    $self->logger->warn("Bad sky value: $weather_info->{sky}");
    $weather_info->cloudless;
  }

  if ( $msg ) {
    foreach my $client ( $self->world->clients ) {
      my $character = $client->character;
      if ( $client->is_playing && $character->is_outside && $character->is_awake ) {
        $character->send($msg);
      }
    }
  }
}

__PACKAGE__->meta->make_immutable;

1;
