package Game::Mud::ROM::MudState::WeatherInfo;
use Modern::Perl;
use Moose;
use namespace::sweep;

has mmhg => (is => 'rw', isa => 'Int', default => 0);
has change => (is => 'rw', isa => 'Int', default => 0);
has sky => (is => 'rw', isa => 'Int', default => 0);
has sunlight => (is => 'rw', isa => 'Int', default => 0);

use constant DAWN => 1;
use constant DAY => 2;
use constant DUSK => 3;
use constant NIGHT => 0;

use constant CLOUDLESS => 0;
use constant CLOUDY => 1;
use constant RAINING => 2;
use constant LIGHTNING => 3;

sub dawn {
  my ($self, $arg) = @_;
  if ( defined $arg ) {
    $self->{sunlight} = !$arg ? 0 : DAWN;
  }

  return $self->{sunlight} == DAWN;
}

sub day {
  my ($self, $arg) = @_;
  if ( defined $arg ) {
    $self->{sunlight} = !$arg ? 0 : DAY;
  }

  return $self->{sunlight} == DAY;
}

sub dusk {
  my ($self, $arg) = @_;
  if ( defined $arg ) {
    $self->{sunlight} = !$arg ? 0 : DUSK;
  }

  return $self->{sunlight} == DUSK;
}

sub night {
  my ($self, $arg) = @_;
  if ( defined $arg ) {
    $self->{sunlight} = !$arg ? 0 : NIGHT;
  }

  return $self->{sunlight} == NIGHT;
}

sub cloudless {
  my ($self, $arg) = @_;
  if ( defined $arg ) {
    $self->{sky} = !$arg ? 0 : CLOUDLESS;
  }

  return $self->{sky} == CLOUDLESS;
}

sub cloudy {
  my ($self, $arg) = @_;
  if ( defined $arg ) {
    $self->{sky} = !$arg ? 0 : CLOUDY;
  }

  return $self->{sky} == CLOUDY;
}

sub raining {
  my ($self, $arg) = @_;
  if ( defined $arg ) {
    $self->{sky} = !$arg ? 0 : RAINING;
  }

  return $self->{sky} == RAINING;
}

sub lightning {
  my ($self, $arg) = @_;
  if ( defined $arg ) {
    $self->{sky} = !$arg ? 0 : LIGHTNING;
  }

  return $self->{sky} == LIGHTNING;
}

__PACKAGE__->meta->make_immutable;

1;
