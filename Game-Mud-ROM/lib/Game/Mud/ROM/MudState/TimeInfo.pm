package Game::Mud::ROM::MudState::TimeInfo;
use Modern::Perl;
use Moose;
use namespace::sweep;

has hour => (is => 'rw', isa => 'Int', default => 0);
has day => (is => 'rw', isa => 'Int', default => 0);
has month => (is => 'rw', isa => 'Int', default => 0);
has year => (is => 'rw', isa => 'Int', default => 0);

sub advance_hour {
  my $self = shift;

  $self->{hour}++;

  if ( $self->{hour} == 24 ) {
    $self->{hour} = 0;
    $self->{day}++;
  }

  if ( $self->{day} >= 35 ) {
    $self->{day} = 0;
    $self->{month}++;
  }

  if ( $self->{month} >= 17 ) {
    $self->{month} = 0;
    $self->{year}++;
  }
}

__PACKAGE__->meta->make_immutable;

1;
