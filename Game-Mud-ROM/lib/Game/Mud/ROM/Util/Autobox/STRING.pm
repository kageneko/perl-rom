package Game::Mud::ROM::Util::Autobox::STRING;
use Modern::Perl;
use namespace::sweep;

use Term::ANSIColor ':constants';

sub index_of {
  my ($string, $match) = @_;
  if ( $string !~ /$match/ ) {
    return -1;
  }

  return $-[0];
}

sub starts_with {
  my ($string, $match) = @_;
  $match = '^' . $match;
  return $string =~ /$match/i;
}

sub ends_with {
  my ($string, $match) = @_;
  $match = $match . '$';
  return $string =~ /$match/i;
}

sub strip_color {
  my $string = shift;
  $string =~ s/\{.//go;
  return $string;
}

sub convert_color {
  my $string = shift;
  $string =~ s/\{(.)/color($1)/ego;
  return $string;
}

my %COLORS = (
  x => CLEAR,
  X => BLACK,
  b => BLUE,
  c => CYAN,
  g => GREEN,
  m => MAGENTA,
  r => RED,
  w => WHITE,
  y => YELLOW,
  B => BRIGHT_BLUE,
  C => BRIGHT_CYAN,
  G => BRIGHT_GREEN,
  M => BRIGHT_MAGENTA,
  R => BRIGHT_RED,
  W => BRIGHT_WHITE,
  Y => BRIGHT_YELLOW,
  D => BRIGHT_BLACK,
  '{' => '{',
);

sub color {
  my ($code) = @_;

  return $COLORS{$code} // CLEAR;
}

1;


