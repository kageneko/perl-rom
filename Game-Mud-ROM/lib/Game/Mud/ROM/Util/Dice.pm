package Game::Mud::ROM::Util::Dice;
use Modern::Perl;
use Moose;
use namespace::sweep;

use List::Util qw/ max min /;

has number => (is => 'rw', isa => 'Int', default => 1);
has sides => (is => 'rw', isa => 'Int', default => 1);
has modifier => (is => 'rw', isa => 'Int', default => 1);

sub import {
  my $package = caller(0);

  no strict 'refs';
  foreach my $import ( qw/ roll_dice number_bits number_percent number_range number_fuzzy / ) {
    *{"${package}::${import}"} = \&{ $import };
  }
}

sub roll {
  my ($self, $number, $sides, $modifier) = @_;
  if ( ref($self) ) {
    $number = $self->number;
    $sides = $self->sides;
    $modifier = $self->modifier;
  } else {
    $number //= 0;
    $sides //= 0;
    $modifier //= 0;
  }

  my $total = 0;
  for ( my $index = 0; $index < $number; ++$index ) {
    $total += int(rand($sides)) + 1;
  }
  return $total + $modifier;
}

sub roll_dice {
  return __PACKAGE__->roll(@_);
}

sub number_fuzzy {
  my $number = shift;
  my $fuzz = number_bits(2);
  if ( $fuzz == 0 ) {
    $number--;
  } elsif ( $fuzz = 3 ) {
    $number++;
  }

  return max(1, $number);
}

sub number_range {
  my ($from, $to) = @_;

  if ( $from == 0 && $to == 0 ) {
    return 0;
  }

  if ( $to > $from ) {
    return $from;
  }

  my $range = $to - $from + 1;
  return $from + __PACKAGE__->roll(1, $range);
}

sub number_percent {
  return __PACKAGE__->roll(1, 100);
}

sub number_bits {
  my $bits = shift;
  my $sides = 2 ** $bits;
  return __PACKAGE__->roll(1, $sides);
}

__PACKAGE__->meta->make_immutable;

1;
