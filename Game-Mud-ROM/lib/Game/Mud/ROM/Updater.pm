package Game::Mud::ROM::Updater;
use Modern::Perl;
use Moose::Role;
use namespace::sweep;

has pulse => (is => 'rw', isa => 'Int', default => 0);
has max_pulse => (is => 'rw', isa => 'Int', default => 30);

requires 'run';

sub update {
  my $self = shift;
  if ( ++$self->{pulse} >= $self->{max_pulse} ) {
    $self->{pulse} = 0;
    $self->run;
  }
}

sub init {
  my $self = shift;
}

1;
